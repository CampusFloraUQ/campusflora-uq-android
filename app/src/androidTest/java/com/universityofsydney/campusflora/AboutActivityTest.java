/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora;

import android.test.ActivityInstrumentationTestCase2;
import android.test.ViewAsserts;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import java.io.IOException;

/**
 * Created by Alex on 22/08/2015.
 */
public class AboutActivityTest extends ActivityInstrumentationTestCase2<AboutActivity> {

    private AboutActivity mActivity;
    private Button mFeedbackButton;

    public AboutActivityTest() {
        super(AboutActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mActivity = getActivity();
    }

    public void testLayout() {
        View decorView = mActivity.getWindow().getDecorView();
        //Get the button
        mFeedbackButton = (Button) mActivity.findViewById(R.id.button_feedback);
        ViewAsserts.assertOnScreen(decorView, mFeedbackButton);

        ViewGroup.LayoutParams layoutParams = mFeedbackButton.getLayoutParams();
        //Check the layout of the button isn't null and that it uses wrap_content.
        assertNotNull(layoutParams);
        assertEquals(layoutParams.width, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals(layoutParams.height, WindowManager.LayoutParams.WRAP_CONTENT);


    }

    public void testWebview() {

        try {
            mActivity.getAssets().open("About.html");
        } catch (IOException e) {
            assertNotNull("Error opening About.html", null);
        }
    }


}