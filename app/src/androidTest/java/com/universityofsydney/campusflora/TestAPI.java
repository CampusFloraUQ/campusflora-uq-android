/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.test.suitebuilder.annotation.SmallTest;

import com.universityofsydney.campusflora.floraAPI.FloraAPI;
import com.universityofsydney.campusflora.floraAPI.Species;

import java.util.List;

/**
 * Created by Alex on 22/08/2015.
 */
public class TestAPI extends ActivityInstrumentationTestCase2<MapsActivity> {

    private MapsActivity mActivity;
    private FloraAPI floraAPI;
    private List<Species> speciesList;

    public TestAPI() {
        super(MapsActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mActivity = getActivity();
        //this.floraAPI = new FloraAPI(mActivity);
//        this.speciesList = floraAPI.getSpeciesList(); //Uncomment stuff in FloraAPI to test this, commented out in
        //FloraAPI for performance purposes.
    }

    @SmallTest
    public void testListSize(){
        /*Found small error in .json files through this test. Some plants don't have their ID's in consecutive order
        Eg. no plants with id 147 exist in the .json file. The last plant in the .json file has id 258 however there are only
        254 plants with id's in the file. Hence we compare to 284
        */
//        assertEquals(258, speciesList.size());
        assertEquals(speciesList.size(), 254);
    }

    @SmallTest
    public void testGetGenusSpeciesNotNull(){
        for(int i = 0; i < speciesList.size(); i++){
            assertNotNull("One of the species has a null species name", speciesList.get(i).getGenusSpecies());;
        }
    }

    @SmallTest
    public void testImageURLSNotNull(){
        for(int i = 0; i < speciesList.size(); i++){
            Species currSpecies = speciesList.get(i);
            //Loop through the images for the current species:
            for(int j = 0; j < currSpecies.getImages().size(); j++){
                for(int k = 0; k < currSpecies.getImages().get(j).size(); k++){
                    String url = currSpecies.getImages().get(j).get(k);
                    assertNotNull("One of the images has a null url", url);
                }
            }
        }
    }

    @MediumTest
    public void testImageURLS(){
        for(int i = 0; i < speciesList.size(); i++){
            Species currSpecies = speciesList.get(i);
            //Loop through the images for the current species:
            for(int j = 0; j < currSpecies.getImages().size(); j++){
                for(int k = 0; k < currSpecies.getImages().get(j).size(); k++){
                    String url = currSpecies.getImages().get(j).get(k);
                    assertNotNull("One of the images has a null url", url);
                }
            }
        }

    }


}
