/*
 * Copyright (C) 2013 Maciej Górski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
* Ammendments made by Campus Flora Android Team.
* */

package com.universityofsydney.campusflora.map;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidmapsextensions.ClusteringSettings;
import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.universityofsydney.campusflora.PlantDetails.PlantDetails;
import com.universityofsydney.campusflora.R;
import com.universityofsydney.campusflora.floraAPI.FloraAPI;
import com.universityofsydney.campusflora.floraAPI.Species;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CampusMapFragment extends BaseFragment {
    protected FloraAPI floraAPI;

    LayoutInflater inflater;

    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.context = getActivity();
        this.inflater = inflater;
        return inflater.inflate(R.layout.campus_map, container, false);
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    @Override
    protected void setUpMap() {
        if (floraAPI == null) {
            floraAPI = floraAPI.getInstance();
        }
        LatLng sydneyUni = new LatLng(-33.888629, 151.187347);
        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydneyUni, 16));
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        map.setClustering(
                new ClusteringSettings().clusterOptionsProvider(
                        new CampusFloraClusterOptionsProvider(getResources())
                )
                        .minMarkersCount(15)
                        .addMarkersDynamically(true)
        );

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                if (marker.isCluster()) {
                    //do nothing
                } else {
                    Intent intent = new Intent(context,
                            PlantDetails.class
                    );
                    intent.putExtra(PlantDetails.EXTRA_MESSAGE, marker.getTitle());
                    startActivity(intent);
                }
            }
        });

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            private TextView tv;

            {
                tv = new TextView(getActivity());
                tv.setTextColor(Color.BLACK);
            }

            private Collator collator = Collator.getInstance();
            private Comparator<Marker> comparator = new Comparator<Marker>() {
                public int compare(Marker lhs, Marker rhs) {
                    String leftTitle = lhs.getTitle();
                    String rightTitle = rhs.getTitle();
                    if (leftTitle == null && rightTitle == null) {
                        return 0;
                    }
                    if (leftTitle == null) {
                        return 1;
                    }
                    if (rightTitle == null) {
                        return -1;
                    }
                    return collator.compare(leftTitle, rightTitle);
                }
            };

            @Override
            public View getInfoWindow(Marker marker) {
                if (marker.isCluster()) {
                    List<Marker> markers = marker.getMarkers();
                    int i = 0;
                    String text = "";
                    while (i < 3 && markers.size() > 0) {
                        Marker m = Collections.min(markers, comparator);
                        String title = m.getTitle();
                        if (title == null) {
                            break;
                        }
                        text += title + "\n";
                        markers.remove(m);
                        i++;
                    }
                    if (text.length() == 0) {
                        text = "Markers with mutable data";
                    } else if (markers.size() > 0) {
                        text += "and " + markers.size() + " more...";
                    } else {
                        text = text.substring(0, text.length() - 1);
                    }
                    tv.setText(text);
                    tv.setBackgroundResource(R.drawable.round_edge);
                    tv.setPadding(30, 20, 30, 20);
                    return tv;
                } else {
                    View v = inflater.inflate(R.layout.marker_info_window, null);

                    TextView title = (TextView) v.findViewById(R.id.marker_title);
                    title.setText(marker.getTitle()); //genus name

                    TextView snippet = (TextView) v.findViewById(R.id.marker_snippet);
                    snippet.setText(floraAPI.getSpecies(marker.getTitle()).getCommonName());

                    TextView arborPlan = (TextView) v.findViewById((R.id.marker_arbourPlan));
                    if (marker.getSnippet().length() != 5) {
                        arborPlan.setEnabled(false);
                        arborPlan.setVisibility(View.INVISIBLE);
                        arborPlan.setText("");
                    } else {
                        arborPlan.setText(
                                String.format("Arborplan: %s", marker.getSnippet())
                        );
                    }
                    ImageView Image = (ImageView) v.findViewById(R.id.marker_icon);
                    Drawable imgDrawable = floraAPI.getLocalThumbnail(marker.getTitle());

                    if (imgDrawable != null) {
                        Bitmap circularBitmap = getCircleBitmap(drawableToBitmap(imgDrawable));

                        Image.setImageDrawable(imgDrawable);
                        Image.setImageBitmap(circularBitmap);
                    }
                    return v;
                }
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });

        //add all plant data to map.
        try {
            for (String genusName : floraAPI.getGenusNames()) {
                Species species = floraAPI.getSpecies(genusName);
                for (Pair<LatLng, String> loc : species.getLocations()) {
                    map.addMarker(new MarkerOptions()
                                    .title(species.getGenusSpecies())
                                    .position(loc.first)
                                    .snippet(loc.second)
                    );
                }
            }
        } catch (Exception e) {
            Log.e(context.getPackageName(), "error occurred");
        }

        //initialize our marker filterer with our markers.
        MarkerFilterer.getInstance().init(map.getMarkers());
    }

    //changes map mode
    public void toggleMap() {
        if (map.getMapType() == GoogleMap.MAP_TYPE_NORMAL) {
            map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        } else if (map.getMapType() == GoogleMap.MAP_TYPE_HYBRID) {
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        } else {
            Log.e(context.getPackageName(),
                    "toggle Maps Error, unknown mode " + map.getMapType()
            );
        }
    }

}