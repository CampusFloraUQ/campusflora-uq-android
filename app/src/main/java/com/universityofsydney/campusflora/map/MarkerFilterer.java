/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora.map;

import android.support.annotation.Nullable;

import com.androidmapsextensions.Marker;
import com.universityofsydney.campusflora.floraAPI.FloraAPI;
import com.universityofsydney.campusflora.floraAPI.Species;
import com.universityofsydney.campusflora.floraAPI.Trail;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MarkerFilterer {
    private List<Marker> markers;
    private Trail currentTrail;
    private Map<String, Boolean> familyStates;
    private static MarkerFilterer instance;

    //subset of markers for the trail.
    private List<Marker> trailMarkers;
    //private boolean isCleared = false;

    //holds whether anything is currently filtered.

    // returns whether a trail is currently active.
    private boolean isTrailEnabled() {
        return !(currentTrail == null);
    }

    public static MarkerFilterer getInstance() {
        if (instance == null) {
            instance = new MarkerFilterer();
        }
        return instance;
    }

    //must be called at least once before use
    public void init(List<Marker> markers) {
        this.markers = markers;
        this.currentTrail = null;
        this.familyStates = new HashMap<>();
    }

    // sets all markers to being visible.
    // if a trail is enabled, only those markers will be visible
    public void clearFilter() {
        if (markers == null) {
            return;
        }

        //get either full set of markers, or if trails are enabled, only the trail markers.
        List<Marker> m = markers;
        if (this.isTrailEnabled()) {
            m = this.trailMarkers;
        }

        for (Marker marker : m) {
            Species species = FloraAPI.getInstance().getSpecies(marker.getTitle());
            if (this.isFamilyEnabled(species.getFamilyName())) {
                marker.setVisible(true);
            } else {
                marker.setVisible(false);
            }
        }

//        this.isCleared = true;
    }

    // returns whether a family should be visible.
    public boolean isFamilyEnabled(String familyName) {
        if (this.familyStates.containsKey(familyName)) {
            return familyStates.get(familyName);
        }
        // we default to true (is visible)
        return true;
    }

    // returns whether any family filtering is occuring,
    // used to determine whether or not to grey out "families" option
    public boolean isFamilyFiltering() {
        for (String key : this.familyStates.keySet()) {
            if (this.familyStates.get(key) == false) {
                return true; //one family is disabled, and therefore we are actively filtering.
            }
        }
        return false;
    }

    // apply a filter from a given string.
    public void applyFilter(String filterText) {
        if (markers == null) {
            return;
        }
        if (filterText.equals("")) {
            this.clearFilter();
            return;
        }

        filterText = filterText.toLowerCase();

        // If trails are enabled, we dont want all the markers.
        List<Marker> m = markers;
        if (isTrailEnabled()) {
            m = this.trailMarkers;
        }

        for (Marker marker : m) {
            Species species = FloraAPI.getInstance().getSpecies(marker.getTitle());
            if (species.getGenusSpecies().toLowerCase().contains(filterText) ||
                    species.getCommonName().toLowerCase().contains(filterText) ||
                    species.getFamilyName().toLowerCase().contains(filterText) ||
                    marker.getSnippet().toLowerCase().contains(filterText) //arborID
                            && this.isFamilyEnabled(species.getFamilyName())
                    ) {
                marker.setVisible(true);
            } else {
                marker.setVisible(false);
            }
        }
//        this.isCleared = false;
    }

    // enables a trail filter on map.
    public void applyTrail(Trail trail) {
//        this.isCleared = false;

        // we will use null to remove a trail.
        if (trail == null) {
            this.currentTrail = null;
            this.trailMarkers = null;
            this.clearFilter();
            return;
        }

        // clear any existing family filter that may be enabled.
        this.clearFamilyFilter();

        this.currentTrail = trail;
        this.trailMarkers = new LinkedList<>();
        Map<String, Species> speciesList = trail.getSpecies();
        for (Marker marker : markers) {
            marker.setVisible(false); // this will be updated later in clearFilter()
            String genusName = marker.getTitle();
            if (trail.getSpecies().keySet().contains(genusName)) {
                this.trailMarkers.add(marker);
            }
        }
        this.clearFilter();
    }

    private void clearFamilyFilter() {
        for (String key : this.familyStates.keySet()) {
            this.familyStates.put(key, true);
        }
    }

    @Nullable
    public Trail getCurrentTrail() {
        return this.currentTrail;
    }

    public void applyFilterByFamily(String familyName, boolean visible) {
        if (markers == null) {
            return;
        }
        // remove any existing trail.
        this.applyTrail(null);
        familyStates.put(familyName, visible);
        //update our filter.
//        isCleared = false;
    }
}
