/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.universityofsydney.campusflora.PlantDetails.PlantDetails;
import com.universityofsydney.campusflora.SpeciesList.ListAdapter;
import com.universityofsydney.campusflora.floraAPI.FloraAPI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FavoriteActivity extends ActionBarActivity {

    private FloraAPI floraAPI;
    private ListAdapter adapter;
    private SharedPreferences favPlants;
    private SharedPreferences.Editor favPlantsEd;
    private ListView listView;
    private List<String> favoritePlants;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        toolbar = (Toolbar) findViewById(R.id.fav_tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Favourites");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.floraAPI = floraAPI.getInstance();
        favPlants = getSharedPreferences("favPlants", 0);
        favPlantsEd = favPlants.edit();
        this.listView = (ListView) findViewById(R.id.favorite_list_view);
        this.favoritePlants = new ArrayList<String>();
        createListView();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        //Update the listview everytime the user presses back
        updateListView();
    }

    public void updateListView() {
        favoritePlants.clear(); //Clear the list first in case the user has unfavorited any plants
        //Loop through all genus names from floraAPI and check if they have been favorited.
        for (String key : floraAPI.getGenusNames()) {
            if (favPlants.getInt(key, 0) == 1) {
                //If it is a favorite add it to the list adapter
                favoritePlants.add(key);
            }
        }
        //If list empty, display "No favorite plants" and give instructions on how to favorite plants
        TextView subtitle = (TextView) findViewById(R.id.favoritesSubtitle);
        ImageView heartIcon = (ImageView) findViewById(R.id.favouriteHeartIcon);
        if (favoritePlants.size() == 0) {
            subtitle.setText(R.string.favouriteHint);
            heartIcon.setVisibility(View.VISIBLE);
        } else {
            subtitle.setText("");
            heartIcon.setVisibility(View.INVISIBLE);
        }
        Collections.sort(favoritePlants);
        //Set the adapter
        adapter = new ListAdapter(getApplicationContext(), 0, favoritePlants);
        listView.setAdapter(adapter);
    }

    public void createListView() {
        //Update the list view
        updateListView();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(FavoriteActivity.this, PlantDetails.class);
                String message = adapter.getItem(position);
                intent.putExtra(PlantDetails.EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favorite, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
