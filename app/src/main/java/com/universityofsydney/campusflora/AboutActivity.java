/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import com.universityofsydney.campusflora.floraAPI.FloraAPI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AboutActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_about);
        super.onCreate(savedInstanceState);

        // on close button click return to main activity
        Button close = (Button) findViewById(R.id.button_close);
        close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        Button buttonSendEmail_intent = (Button) findViewById(R.id.button_feedback);

        try {
            setAboutText();
        } catch (IOException e) {
            Log.e(getPackageName(), "Error opening About.html");
        }

        buttonSendEmail_intent.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "campus.flora@sydney.edu.au", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Campus Flora Android feedback");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(emailIntent, "Please choose an email client"));
            }
        });
    }

    private void setAboutText() throws IOException

    {
        WebView webView = (WebView) findViewById(R.id.web_view);
        String numberOfFamilies = String.valueOf(FloraAPI.getInstance().getNumberOfFamilies());
        String numberOfSpecies = String.valueOf(FloraAPI.getInstance().getGenusNames().size());
        String numberOfPlants = String.valueOf(FloraAPI.getInstance().getNumberOfPlants());

        // god bless java.
        BufferedReader htmlFile = new BufferedReader(
                new InputStreamReader(
                        getAssets().open("About.html")
                )
        );
        String htmlString = "";
        String str;
        while ((str = htmlFile.readLine()) != null) {
            htmlString += str;
        }

        // replace fields in the string with live values.
        htmlString = htmlString
                .replace("{N_OF_FAMILIES}", numberOfFamilies)
                .replace("{N_OF_SPECIES}", numberOfSpecies)
                .replace("{N_OF_PLANTS}", numberOfPlants);


        webView.loadData(htmlString, "text/html; charset=UTF-8", null);
    }
}
