/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora.PlantDetails;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.universityofsydney.campusflora.MapsActivity;
import com.universityofsydney.campusflora.R;
import com.universityofsydney.campusflora.floraAPI.FloraAPI;
import com.universityofsydney.campusflora.floraAPI.Species;
import com.universityofsydney.campusflora.map.MarkerFilterer;

import java.util.ArrayList;
import java.util.List;

public class PlantDetails extends ActionBarActivity {
    public final static String EXTRA_MESSAGE = "com.universityofsydney.campusflora.MESSAGE";
    private final static int MOBILE = 2;

    public static int image_ID;
    MyPagerAdapter myPagerAdapter;
    ImageView imageView;

    public static FloraAPI floraAPI;
    public static Species plant;
    public static String genusName;
    private ShareActionProvider mShareActionProvider;

    String urlShare;

    private boolean favClicked;
    private ImageButton favoriteBtn;
    private SharedPreferences favPlants;
    private SharedPreferences.Editor favPlantsEd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        genusName = intent.getStringExtra(PlantDetails.EXTRA_MESSAGE);
        String nameDashed = genusName.replace(".", "").replace("'", "").replace(" ", "-");
        urlShare = "campusflora.sydneybiology.org/species/" + nameDashed;

        floraAPI = FloraAPI.getInstance();

        if (genusName == null) {
            Log.e(this.getPackageName(), "ERROR intent empty " + intent.toString());
            genusName = "";
        }
        setContentView(R.layout.activity_plant_details);
        setActionBar(genusName);

        ViewPager viewPager = (ViewPager) findViewById(R.id.pda_view_pager);
        myPagerAdapter = new MyPagerAdapter();
        viewPager.setAdapter(myPagerAdapter);

        favPlants = getSharedPreferences("favPlants", 0);
        favPlantsEd = favPlants.edit();

        setActionBar(genusName);
        setTexts(genusName);

        Button findMarker = (Button) findViewById(R.id.btn_show_on_map);
        findMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MarkerFilterer.getInstance().applyFilter(genusName);
                MapsActivity maps = (MapsActivity) MapsActivity.context;
                maps.setFilter(genusName);

                Intent i = new Intent(PlantDetails.this, MapsActivity.class);
                // this intent tries to restore maps from memory
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);
                finish();
            }
        });
    }

    private void setActionBar(String genusName) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.plant_details_tool_bar);
        toolbar.setTitleTextAppearance(this, R.style.ItalicsToolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(genusName);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_plant_details, menu);

        MenuItem shareItem = menu.findItem(R.id.plants_details_share);

        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, urlShare);
        setShareIntent(shareIntent);

        return true;
    }

    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private class MyPagerAdapter extends PagerAdapter {

        FloraAPI myFlora = PlantDetails.floraAPI;
        Species myPlant = myFlora.getSpecies(PlantDetails.genusName);
        List<ArrayList<String>> myImages = myPlant.getImages();
        int NumberOfPages = myImages.size();

        @Override
        public int getCount() {
            return NumberOfPages;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            setTitle(genusName);

            Log.d("plant_details_act", "Checking Log");

            if (NumberOfPages >= 1) {
                imageView = new ImageView(PlantDetails.this);
                Picasso.with(PlantDetails.this).load(myImages.get(position).get(MOBILE)).into(imageView);
                imageView.setLayoutParams(new ViewGroup.MarginLayoutParams
                        (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            } else {
                Log.d("empty plant image", plant.getGenusSpecies());
                imageView = new ImageView(PlantDetails.this);
                imageView.setImageResource(R.drawable.img_not_found);
                imageView.setLayoutParams(new ViewGroup.MarginLayoutParams
                        (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            }

            LinearLayout layout = new LinearLayout(PlantDetails.this);
            layout.setOrientation(LinearLayout.HORIZONTAL);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layout.setLayoutParams(layoutParams);
            layout.addView(imageView);

            final int id = position;
            layout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    image_ID = id;
                    Log.d("image_ID", Integer.toString(id));
                    Log.d("image_ID", Integer.toString(image_ID));
                    Intent intent = new Intent(PlantDetails.this, ImageActivity.class);
                    String message = myPlant.getGenusSpecies();
                    intent.putExtra(EXTRA_MESSAGE, message);
                    startActivity(intent);
                }
            });
            container.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

    private void setTexts(final String genusName) {
        //Setting information text of a specie
        plant = floraAPI.getSpecies(genusName);
        TextView tv = (TextView) findViewById(R.id.genus_detail);
        tv.setText(genusName + " ");

        tv = (TextView) findViewById(R.id.authority_line);
        tv.setText(plant.getAuthority());

        tv = (TextView) findViewById(R.id.common_name);
        tv.setText("Common Name: " + plant.getCommonName());

        tv = (TextView) findViewById(R.id.family_name);
        tv.setText("Family Name: " + plant.getFamilyName());

        tv = (TextView) findViewById(R.id.description);
        tv.setText(plant.getDescription());
        tv.setMovementMethod(LinkMovementMethod.getInstance());

        tv = (TextView) findViewById(R.id.distribution);
        tv.setText(plant.getDistribution());

        tv = (TextView) findViewById(R.id.phylogeny);
        tv.setText(plant.getPhylogeny());

        tv = (TextView) findViewById(R.id.information);
        tv.setText(plant.getInformation());
        tv.setMovementMethod(LinkMovementMethod.getInstance());

        //Favorite Button stuff

        favoriteBtn = (ImageButton) findViewById(R.id.favoriteButton);
        //If user has favorited this plant then display coloured in star, else display empty star
        if (favPlants.getInt(genusName, 0) == 1) {
            favoriteBtn.setBackgroundResource(R.drawable.ic_favorite_black_24dp);
            favClicked = true;
        } else {
            favoriteBtn.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp);
            favClicked = false;
        }

        View.OnClickListener favListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (favClicked) {
                    //If user clicks to unfavorite
                    favPlantsEd.putInt(genusName, 0);
                    favoriteBtn.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp);
                    favPlantsEd.commit();
                } else {
                    //If user clicks to favorite then set number to 1
                    favPlantsEd.putInt(genusName, 1);
                    favoriteBtn.setBackgroundResource(R.drawable.ic_favorite_black_24dp);
                    favPlantsEd.commit();
                }
                favClicked = !favClicked;
            }
        };
        favoriteBtn.setOnClickListener(favListener);
    }
}
