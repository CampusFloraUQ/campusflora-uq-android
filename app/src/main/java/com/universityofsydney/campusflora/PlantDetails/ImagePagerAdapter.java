/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora.PlantDetails;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class ImagePagerAdapter extends PagerAdapter {

    private ArrayList<ImageView> views = new ArrayList<ImageView>();

    // Adds a view.
    public void addView(ImageView view) {
        views.add(view);
    }

    // Returns the view in specified position.
    public View getView(int position) {
        return views.get(position);
    }

    // Create the page for the given position.
    // The adapter is responsible for adding the view to the container given here,
    // although it only must ensure this is done by the time it returns from finishUpdate(ViewGroup).
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = views.get(position);
        container.addView(view);

        return view;
    }

    // Remove a page for the given position.
    // The adapter is responsible for removing the view from its container,
    // although it only must ensure this is done by the time it returns from finishUpdate(ViewGroup).
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views.get(position));
    }

    // Return the number of views available.
    @Override
    public int getCount() {
        return views.size();
    }

    // Determines whether a page View is associated with a specific key object as returned by instantiateItem(ViewGroup, int).
    // This method is required for a PagerAdapter to function properly.
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
