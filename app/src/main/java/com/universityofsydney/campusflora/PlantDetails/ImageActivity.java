/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora.PlantDetails;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.squareup.picasso.Picasso;
import com.universityofsydney.campusflora.R;
import com.universityofsydney.campusflora.floraAPI.FloraAPI;
import com.universityofsydney.campusflora.floraAPI.Species;

import java.util.ArrayList;
import java.util.List;

public class ImageActivity extends Activity {
    private FloraAPI floraAPI;
    private Species plant;
    private ImagePagerAdapter imgPgrAdpt;
    private ViewPager viewPager;

    private static final int DEFAULT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        floraAPI = FloraAPI.getInstance();

        setContentView(R.layout.activity_image);

        String genusName = intent.getStringExtra(PlantDetails.EXTRA_MESSAGE);
        if (genusName == null) {
            Log.e(this.getPackageName(), "ERROR intent empty " + intent.toString());
            genusName = "";
        }

        this.plant = floraAPI.getSpecies(genusName);
        setImages();
    }

    // Sets all of images of a plant into a pager. Pager is able to swipe to view multiple images.
    // images are zoomable by pinching.
    private void setImages() {

        imgPgrAdpt = new ImagePagerAdapter();
        viewPager = (ViewPager) findViewById(R.id.zoomable_view_pager);
        viewPager.setAdapter(imgPgrAdpt);

        List<ArrayList<String>> images = this.plant.getImages();

        for (int i = 0; i < this.plant.getImages().size(); ++i) {
            TouchImageView imageView = new TouchImageView(this);
            Picasso.with(this).load(images.get(i).get(DEFAULT)).into(imageView);
            imageView.setMaxZoom(4f);
            imgPgrAdpt.addView(imageView);
            imgPgrAdpt.notifyDataSetChanged();
        }
        int getImageId = PlantDetails.image_ID;
        viewPager.setCurrentItem(getImageId);
    }
}
