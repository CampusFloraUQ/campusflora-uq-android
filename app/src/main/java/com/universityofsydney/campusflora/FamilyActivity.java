/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.universityofsydney.campusflora.floraAPI.FloraAPI;
import com.universityofsydney.campusflora.map.MarkerFilterer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FamilyActivity extends ActionBarActivity {

    private FloraAPI floraAPI;
    private Family[] families;
    private ArrayAdapter<Family> listAdapter;
    private ListView familyListView;
    private ArrayList<Family> familyList = new ArrayList<Family>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        floraAPI = FloraAPI.getInstance();
        setContentView(R.layout.activity_families);

        Toolbar toolbar = (Toolbar) findViewById(R.id.family_tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Families");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setFamilyName();
        setSelectButton();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();
        MapsActivity maps = (MapsActivity) MapsActivity.context;
        maps.setFamilyFilter(MarkerFilterer.getInstance().isFamilyFiltering());
        MarkerFilterer.getInstance().clearFilter();
    }

    private void setFamilyName() {

        familyListView = (ListView) findViewById(R.id.family_ListView);

        familyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View item,
                                    int position, long id) {
                Family family = listAdapter.getItem(position);
                family.toggleChecked();
                FamilyViewHolder viewHolder = (FamilyViewHolder) item.getTag();
                viewHolder.getCheckBox().setChecked(family.isChecked());
                if (family.isChecked()) {
                    MarkerFilterer.getInstance().applyFilterByFamily(family.getName(), true);
                } else {
                    MarkerFilterer.getInstance().applyFilterByFamily(family.getName(), false);
                }
            }
        });

        // Use existing family data and push it onto the list view
        // instead of creating a new one if user has entered this page before
        familyListView.setAdapter(listAdapter);

        // Find and create new family object for each family and sort them alphabetically
        families = (Family[]) getLastCustomNonConfigurationInstance();
        Set<String> genusNames = floraAPI.getGenusNames();

        Map<String, Integer> familyCounts = floraAPI.getFamilyCount();
        ArrayList<String> familyNames = new ArrayList<>(familyCounts.keySet());
        Collections.sort(familyNames);

        // Add family objects
        for (String familyName : familyNames) {
            // family name and family name count
            familyList.add(new Family(familyName, familyCounts.get(familyName)));
        }

        listAdapter = new FamilyArrayAdapter(this, familyList);
        familyListView.setAdapter(listAdapter);

        // update checked value from the marker filter states.
        for (int i = 0; i < familyList.size(); ++i) {
            Family family = listAdapter.getItem(i);
            family.setChecked(
                    MarkerFilterer.getInstance().isFamilyEnabled(family.getName())
            );
        }

    }

    private void setSelectButton() {
        final Button selectAll = (Button) findViewById(R.id.Select_All);
        selectAll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                selectButtonAction(selectAll);
            }
        });

        final Button unSelectAll = (Button) findViewById(R.id.Unselect_All);
        unSelectAll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                selectButtonAction(unSelectAll);
            }
        });
    }

    private void selectButtonAction(Button button) {
        switch (button.getId()) {
            case R.id.Select_All:
                for (int i = 0; i < familyList.size(); i++) {
                    listAdapter.getItem(i).setChecked(true);
                    MarkerFilterer.getInstance()
                            .applyFilterByFamily(listAdapter.getItem(i).getName(), true);
                }
                // Refreshing the list view when button is clicked
                listAdapter.notifyDataSetChanged();
                break;
            case R.id.Unselect_All:
                for (int i = 0; i < familyList.size(); i++) {
                    listAdapter.getItem(i).setChecked(false);
                    MarkerFilterer.getInstance()
                            .applyFilterByFamily(listAdapter.getItem(i).getName(), false);
                }
                // Refreshing the list view when button is clicked
                listAdapter.notifyDataSetChanged();
                break;
            default:
                return;
        }
    }

    private static class Family {
        private String name = "";
        private boolean checked = true;
        int count;

        public Family() {
        }

        public Family(String name, int count) {
            this.name = name;
            this.count = count;
        }

        public Family(String name, boolean checked) {
            this.name = name;
            this.checked = checked;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public String toString() {
            return String.format("%s (%d)", name, count);
        }

        public void toggleChecked() {
            checked = !checked;
        }
    }

    private static class FamilyViewHolder {
        private CheckBox checkBox;
        private TextView textView;

        public FamilyViewHolder() {
        }

        public FamilyViewHolder(TextView textView, CheckBox checkBox) {
            this.checkBox = checkBox;
            this.textView = textView;
        }

        public CheckBox getCheckBox() {
            return checkBox;
        }

        public void setCheckBox(CheckBox checkBox) {
            this.checkBox = checkBox;
        }

        public TextView getTextView() {
            return textView;
        }

        public void setTextView(TextView textView) {
            this.textView = textView;
        }
    }

    private static class FamilyArrayAdapter extends ArrayAdapter<Family> {

        private LayoutInflater inflater;

        public FamilyArrayAdapter(Context context, List<Family> planetList) {
            super(context, R.layout.families_list_item, R.id.family_TextView, planetList);
            // Cache the LayoutInflate to avoid asking for a new one each time.
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Family family = this.getItem(position);

            // The child views in each row.
            CheckBox checkBox;
            TextView textView;

            // Create a new row view
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.families_list_item, null);

                // Find the child views.
                textView = (TextView) convertView.findViewById(R.id.family_TextView);
                checkBox = (CheckBox) convertView.findViewById(R.id.family_CheckBox);

                // Optimization: Tag the row with it's child views, so we don't have to
                // call findViewById() later when we reuse the row.
                convertView.setTag(new FamilyViewHolder(textView, checkBox));
            }
            // Reuse existing row view
            else {
                // Because we use a ViewHolder, we avoid having to call findViewById().
                FamilyViewHolder viewHolder = (FamilyViewHolder) convertView.getTag();
                checkBox = viewHolder.getCheckBox();
                textView = viewHolder.getTextView();
            }

            checkBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    Family family = (Family) cb.getTag();
                    family.toggleChecked();
                    // When a box is checked, and is clicked, set the marker invisible on map
                    // else when a box is unchecked, and is clicked, set marker visible on map
                    if (family.isChecked()) {
                        MarkerFilterer.getInstance().applyFilterByFamily(family.getName(), true);
                    } else {
                        MarkerFilterer.getInstance().applyFilterByFamily(family.getName(), false);
                    }

                }
            });
            // Tag the CheckBox with the family
            //it is displaying, so that we can
            // access the family
            //in onClick() when the CheckBox is toggled.
            checkBox.setTag(family);

            // Display family
            //data
            checkBox.setChecked(family.isChecked());
            textView.setText(family.toString());

            return convertView;
        }
    }

    public Object onRetainCustomNonConfigurationInstance() {
        return families;
    }
}
