/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.util.Log;

import com.universityofsydney.campusflora.floraAPI.FloraAPI;
import com.universityofsydney.campusflora.floraAPI.Species;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

// Provides suggestions for the species search view in MapsActivity.
public class SpeciesSearchSuggestionProvider extends ContentProvider {

    String[] columns = {
            "_ID",
            SearchManager.SUGGEST_COLUMN_TEXT_1,
            SearchManager.SUGGEST_COLUMN_TEXT_2,
    };

    FloraAPI floraAPI;

    @Override
    public boolean onCreate() {
        floraAPI = FloraAPI.getInstance();
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder
    ) {
        String query = uri.getLastPathSegment();
        // the query takes the form :
        // content://your.authority/optional.suggest.path/SUGGEST_URI_PATH_QUERY/puppies
        // therefore when the query is empty, the last path segment would be "search_suggest_query"

        MatrixCursor cursor = new MatrixCursor(columns);

        if (query.equals("search_suggest_query")) {
            return cursor; //return empty list.
            //we only want to show suggestions when user has typed
        }

        query = query.toUpperCase();

        int i = 1;


        //sort species in alphabetical order.
        Set<String> genusNames = floraAPI.getGenusNames();
        List<String> sortedGenusNames = new ArrayList<>(genusNames);
        Collections.sort(sortedGenusNames);

        for (String genus : sortedGenusNames) {
            Species species = floraAPI.getSpecies(genus);
            if (
                    genus.toUpperCase().contains(query) ||
                            species.getCommonName().toUpperCase().contains(query) ||
                            species.getFamilyName().toUpperCase().contains(query)
                    ) {
                cursor.addRow(new Object[]{
                        i++,
                        genus,
                        species.getCommonName()
                });
            }
        }

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        Log.e(getContext().getPackageName(), "getType");
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.e(getContext().getPackageName(), "insert");
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Log.e(getContext().getPackageName(), "delete");
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.e(getContext().getPackageName(), "update");
        return 0;
    }
}
