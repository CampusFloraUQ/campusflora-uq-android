/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.universityofsydney.campusflora.floraAPI.FloraAPI;
import com.universityofsydney.campusflora.floraAPI.Trail;
import com.universityofsydney.campusflora.map.MarkerFilterer;

import java.util.List;

public class TrailsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trails);

        Toolbar toolbar = (Toolbar) findViewById(R.id.trails_tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Trails");


        populateTrails();


        //ListView listViewTrails = (ListView) findViewById(R.id.lv_trails);


    }

    private void populateTrails() {
        ListView listViewTrails = (ListView) findViewById(R.id.lv_trails);

        String[] trailsArray;
        List<Trail> trails = FloraAPI.getInstance().getTrails();
        trailsArray = new String[trails.size()];
        for (int i = 0; i < trails.size(); ++i) {
            trailsArray[i] = trails.get(i).getTitle();
        }


//        ArrayAdapter<String> adapter = new ArrayAdapter<>(
//                this,
//                R.layout.trails_list_item,
//                R.id.txt_trail_title,
//                trailsArray
//        );

        TrailAdapter adapter = new TrailAdapter(this, trails);

        //adapter.setDropDownViewResource(R.layout.trails_list_item);
        listViewTrails.setAdapter(adapter);

        listViewTrails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List<Trail> trails = FloraAPI.getInstance().getTrails();
                MarkerFilterer.getInstance().applyTrail(trails.get(position));

                MapsActivity maps = (MapsActivity) MapsActivity.context;
                maps.setTrail(true);

                finish();
            }
        });

        MarkerFilterer.getInstance().getCurrentTrail();

        TextView tv = (TextView) findViewById(R.id.clearTrailsButton);

        tv.setOnClickListener(new TextView.OnClickListener() {

            @Override
            public void onClick(View view) {
                MarkerFilterer.getInstance().applyTrail(null);

                MapsActivity maps = (MapsActivity) MapsActivity.context;
                maps.setTrail(false);

                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_trails, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static class TrailView {
        public String title;
        public boolean selected;

        public TrailView() {
            this.title = "";
            this.selected = false;
        }

        public TrailView(String title) {
            this.title = title;
            this.selected = false;
        }
    }

    public class TrailAdapter extends ArrayAdapter<Trail> {
        public TrailAdapter(Context context, List<Trail> trails) {
            super(context, 0, trails);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Trail trail = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(
                        getContext()).inflate(R.layout.trails_list_item, parent, false);

                if (getItem(position) == MarkerFilterer.getInstance().getCurrentTrail()) {
                    convertView.setBackgroundColor(Color.parseColor("#DEDEDE"));
                }
            }
            TextView title = (TextView) convertView.findViewById(R.id.txt_trail_title);
            title.setText(trail.getTitle());

            return convertView;

        }
    }

}
