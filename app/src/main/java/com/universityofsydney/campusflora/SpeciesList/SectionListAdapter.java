/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora.SpeciesList;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.universityofsydney.campusflora.R;
import com.universityofsydney.campusflora.floraAPI.FloraAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SectionListAdapter extends BaseAdapter implements Filterable {

    private static final int TYPE_LIST_ITEM = 0;
    private static final int TYPE_LIST_HEADER = 1;
    private static final int TYPE_COUNT = TYPE_LIST_HEADER + 1;
    FloraAPI floraAPI;
    Context context;

    // viewholder to cache
    private static class ViewHolder {
        TextView genus;
        TextView common;
        TextView header;
        ImageView thumbnail;
    }


    private LayoutInflater layoutInflater;

    private Map<String, List<String>> sections =
            new LinkedHashMap<>();

    private Map<String, List<String>> filteredSections =
            new LinkedHashMap<>();

    private Filter filter;

    public SectionListAdapter(Context context) {
        this.floraAPI = floraAPI.getInstance();
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addSection(String header, List<String> items) {
        this.sections.put(header, items);
        this.filteredSections.put(header, items);

        notifyDataSetChanged();
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = -1;

        for (String header : this.filteredSections.keySet()) {

            List<String> items = this.filteredSections.get(header);
            int sectionCount = items.size() + 1; // Number of items + header

            if (position < sectionCount) {
                viewType = position == 0 ? TYPE_LIST_HEADER : TYPE_LIST_ITEM;
                return viewType;
            } else {
                position -= sectionCount;
            }

        }

        return -1;
    }

    @Override
    public int getCount() {
        int totalCount = 0;

        for (List<String> stations : this.filteredSections.values()) {
            totalCount += stations.size() + 1;
        }

        return totalCount;
    }

    @Override
    public Object getItem(int position) {
        Object item;

        for (String header : this.filteredSections.keySet()) {
            List<String> items = this.filteredSections.get(header);
            int sectionCount = items.size() + 1;

            if (position < sectionCount) {
                item = position == 0 ? header : items.get(position - 1);
                return item;
            } else {
                position -= sectionCount;
            }
        }

        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public boolean isHeader(String item) {

        for (String name : floraAPI.getGenusNames()) {
            if (item.toLowerCase().equals(name.toLowerCase())) {
                return false;
            }
        }

        return true;

    }

    public String getCommonName(String genusName) {
        String commonName;
        try {
            commonName = floraAPI.getSpecies(genusName).getCommonName();
        } catch (Exception e) {
            commonName = "error";
        }
        return commonName;
    }


    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String item = this.getItem(position).toString();
        String commonName = getCommonName(item);
        Boolean isHeader = isHeader(item);
        ViewHolder viewHolder;


        if (convertView == null) {
            viewHolder = new ViewHolder();

            int resourceId = isHeader ?
                    R.layout.species_family_layout :
                    R.layout.species_genus_layout;

            convertView = this.layoutInflater.inflate(resourceId, parent, false);

            if (isHeader) {
                viewHolder.header = (TextView) convertView.findViewById(R.id.family_name);

            } else {
                viewHolder.genus = (TextView) convertView.findViewById(R.id.genus_name);
                viewHolder.genus.setTypeface(null, Typeface.BOLD_ITALIC); //set genus name to italics
                viewHolder.common = (TextView) convertView.findViewById(R.id.common_name);
                viewHolder.thumbnail = (ImageView) convertView.findViewById(R.id.image_thumbnail);

                viewHolder.thumbnail.setId(position);
            }

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (isHeader) {
            viewHolder.header.setText(item);
        } else {
            viewHolder.genus.setText(item);
            viewHolder.common.setText(commonName);
            Drawable currentImage = floraAPI.getLocalThumbnail(item);
            if (currentImage != null) {
                Bitmap circularBitmap = getCircleBitmap(drawableToBitmap(currentImage));
                viewHolder.thumbnail.setImageBitmap(circularBitmap);

            } else {

                Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.no_photo);
                viewHolder.thumbnail.setImageBitmap(getCircleBitmap(bm));
            }

        }


        return convertView;
    }


    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }


    @Override
    public Filter getFilter() {

        if (this.filter == null) {
            this.filter = new dataFilter();
        }

        return this.filter;
    }


    private class dataFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                Map<String, List<String>> newSections =
                        new LinkedHashMap<>();

                for (String header : sections.keySet()) {
                    List<String> data = sections.get(header);
                    List<String> filteredData = new ArrayList<>();

                    for (String name : data) {
                        if (name.toLowerCase().contains(constraint.toString().toLowerCase())) {
                            filteredData.add(name);
                        }
                    }

                    if (filteredData.size() > 0) {
                        newSections.put(header, filteredData);
                    }
                }
                filterResults.values = newSections;
                filterResults.count = newSections.size();
            } else {
                filterResults.values = sections;
                filterResults.count = sections.size();
            }

            return filterResults;
        }

        @Override
        @SuppressWarnings("unchecked")
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {
            Map<String, List<String>> filteredValues =
                    (Map<String, List<String>>) filterResults.values;
            Map<String, List<String>> tempSections = new HashMap<>();
            tempSections.clear();
            filteredSections.clear();
            tempSections.putAll(filteredValues);
            filteredSections = new TreeMap<>(tempSections);

            notifyDataSetChanged();
        }
    }
}