/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora.SpeciesList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.universityofsydney.campusflora.PlantDetails.PlantDetails;
import com.universityofsydney.campusflora.R;
import com.universityofsydney.campusflora.floraAPI.FloraAPI;
import com.universityofsydney.campusflora.floraAPI.Species;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

public class SpeciesListActivity extends ActionBarActivity {

    private FloraAPI floraAPI;
    private SectionListAdapter adapter;
    private ListView listView;
    private ArrayList<String> familyNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_species_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.species_tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Species List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.floraAPI = floraAPI.getInstance();
        listView = (ListView) findViewById(R.id.list_view);

        createListView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createListView() {

        adapter = new SectionListAdapter(this);
        HashMap<String, ArrayList<String>> familyMap = new HashMap<>();
        familyNames = new ArrayList<>();
        Set<String> genusNames = floraAPI.getGenusNames();
        //Loop through each genus and add it to the family it is under
        for (String gName : genusNames) {
            Species species = floraAPI.getSpecies(gName);

            String familyName = species.getFamilyName().trim();

            if (familyMap.get(familyName) != null) {
                //Add the genus species under each family if the family is already in the hashMap
                familyMap.get(familyName).add(species.getGenusSpecies());
            } else {
                //If this family doesn't exist in the hashmap yet, create new arraylist and add it
                familyNames.add(familyName);

                ArrayList<String> newList = new ArrayList<String>();
                newList.add(species.getGenusSpecies());
                familyMap.put(familyName, newList);
            }
        }

        //Sort by genusName in each family after we're done inserting all the species
        for (String key : familyMap.keySet()) {
            Collections.sort(familyMap.get(key));
        }

        //Also sort the by family name over the whole map
        Collections.sort(familyNames);

        //Add the families into the list
        for (int i = 0; i < familyNames.size(); i++) {
            if (adapter != null) {
                adapter.addSection(
                        familyNames.get(i).trim(),
                        familyMap.get(familyNames.get(i))
                );
            }
        }

        listView.setAdapter(adapter);
        EditText inputSearch = (EditText) findViewById(R.id.search_text);


        // search through list of genus species
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(SpeciesListActivity.this, PlantDetails.class);

                String current = (String) listView.getItemAtPosition(position);

                if (!familyNames.contains(current)) {

                    intent.putExtra(PlantDetails.EXTRA_MESSAGE, current);
                    startActivity(intent);
                }
            }
        });
    }
}
