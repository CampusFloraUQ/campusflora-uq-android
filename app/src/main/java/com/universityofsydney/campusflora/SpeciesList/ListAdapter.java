/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora.SpeciesList;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.universityofsydney.campusflora.R;
import com.universityofsydney.campusflora.floraAPI.FloraAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListAdapter extends ArrayAdapter<String> {
    private FloraAPI floraAPI;
    private Context context;
    private List<String> genusList;
    private List<String> genusFilterList;
    genusFilter filter;

    // view caching
    private static class ViewHolder {
        TextView genus;
        TextView common;
        ImageView thumbnail;
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }


    public ListAdapter(Context context, int resource, List<String> genusList) {
        super(context, resource, genusList);
        this.context = context;
        this.genusList = genusList;
        genusFilterList = genusList;
        this.floraAPI = floraAPI.getInstance();
    }

    @Override
    public int getCount() {
        return genusList.size();
    }

    @Override
    public String getItem(int position) {
        return genusList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return genusList.indexOf(getItem(position));
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new genusFilter();
        }
        return filter;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String data = genusList.get(position);
        String common = getCommonName(data);

        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.species_genus_layout, parent, false);

            viewHolder.genus = (TextView) convertView.findViewById(R.id.genus_name);
            viewHolder.genus.setTypeface(null, Typeface.BOLD_ITALIC); //set genus name to italics
            viewHolder.common = (TextView) convertView.findViewById(R.id.common_name);
            viewHolder.thumbnail = (ImageView) convertView.findViewById(R.id.image_thumbnail);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.genus.setText(data);
        viewHolder.common.setText(common);
        Drawable currentImage = floraAPI.getLocalThumbnail(data);
        if (currentImage != null) {
            Bitmap circularBitmap = getCircleBitmap(drawableToBitmap(currentImage));
            viewHolder.thumbnail.setImageBitmap(circularBitmap);
        } else {
            Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.no_photo);
            viewHolder.thumbnail.setImageBitmap(getCircleBitmap(bm));
        }

        return convertView;
    }

    public String getCommonName(String genusName) {
        String commonName;
        try {
            commonName = floraAPI.getSpecies(genusName).getCommonName();
        } catch (Exception e) {
            commonName = "error";
            Log.e(context.getPackageName(), "error getting common name");
            Log.e(context.getPackageName(), e.toString());
        }
        return commonName;
    }

    private class genusFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            constraint = constraint.toString().toLowerCase(Locale.getDefault());
            ArrayList<String> filterList = new ArrayList<String>();


            if (constraint != null && constraint.length() > 0) {

                for (int i = 0; i < genusFilterList.size(); i++) {
                    if ((genusFilterList.get(i).toLowerCase()).contains(constraint.toString().toLowerCase())) {
                        String species = genusFilterList.get(i);

                        filterList.add(species);
                    }
                }

                results.count = filterList.size();
                results.values = filterList;

            } else {
                results.count = genusFilterList.size();
                results.values = genusFilterList;
            }

            return results;
        }

        @Override
        // results.values is 100% a list of strings.
        // we can see that from the context of this class.
        @SuppressWarnings("unchecked")
        protected void publishResults(CharSequence constraint, FilterResults results) {
            genusList = (List<String>) results.values;
            notifyDataSetChanged();
        }
    }


}
