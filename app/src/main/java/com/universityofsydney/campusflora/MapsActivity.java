/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.universityofsydney.campusflora.SpeciesList.SpeciesListActivity;
import com.universityofsydney.campusflora.floraAPI.FloraAPI;
import com.universityofsydney.campusflora.map.CampusMapFragment;
import com.universityofsydney.campusflora.map.MarkerFilterer;

public class MapsActivity extends ActionBarActivity {
    private Toolbar toolbar;

    String TITLES[] = {
            "Species",
            "Families",
            "Trails",
            "Favorites"
    };

    private CampusMapFragment campusMapFragment;
    public static Context context;

    int ICONS[] = {
            R.drawable.ic_view_list_black_48dp,
            R.drawable.ic_device_hub_black_48dp,
            R.drawable.ic_polymer_black_48dp,
            R.drawable.ic_favorite_black_48dp
    };

    String NAME = "Campus Flora";

    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    DrawerLayout Drawer;
    ActionBarDrawerToggle mDrawerToggle;

    public void setTrail(boolean show) {
        SidebarAdapter s = (SidebarAdapter) mAdapter;
        hideFilterButton(); //hides "clear filter" button if shown
        if (show) {
            s.unHighlightItem(1); // unhighlight families
            s.highlightItem(2); // highlight trails.
        } else {
            s.unHighlightItem(2);
        }
    }

    public void setFamilyFilter(boolean show) {
        SidebarAdapter s = (SidebarAdapter) mAdapter;
        hideFilterButton();
        if (show) {
            s.highlightItem(1); // highlight families
            s.unHighlightItem(2); // unhiglight trails
        } else {
            s.unHighlightItem(1);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Initialize our floraAPI.
        if (!FloraAPI.getInstance().isLoaded()) {
            FloraAPI.getInstance().init(getApplicationContext());
        }

        MapsActivity.context = this;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //set actionbar to our custom one.
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        //set up the drawer menu.
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new SidebarAdapter(TITLES, ICONS, NAME);
        mRecyclerView.setAdapter(mAdapter);

        final GestureDetector mGestureDetector = new GestureDetector(
                MapsActivity.this,
                new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        return true;
                    }
                }
        );

        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                    switch (recyclerView.getChildPosition(child)) {
                        case 1: //listview
                            startActivity(new Intent(MapsActivity.this, SpeciesListActivity.class));
                            break;
                        case 2: //family
                            startActivity(new Intent(MapsActivity.this, FamilyActivity.class));
                            break;
                        case 3: //trails
                            startActivity(new Intent(MapsActivity.this, TrailsActivity.class));
                            break;
                        case 4: //Favorites page
                            startActivity(new Intent(MapsActivity.this, FavoriteActivity.class));
                            break;
                        default:
                            break;
                    }
                    Drawer.closeDrawers();
                    return true;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }
        });

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                Drawer,
                toolbar,
                R.string.openDrawer,
                R.string.closeDrawer) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        //apply drawer
        Drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        //apply map fragment
        campusMapFragment = new CampusMapFragment();
        setMapFragment(campusMapFragment);
    }

    private SearchView searchView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_maps, menu);

        //initialize search
        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        searchView = (SearchView) searchItem.getActionView();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (null != searchManager) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(false);

        // on every text update, we will re-apply the filter.
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                //We dont need to do anything on submit, since filtering occurs live.
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                MarkerFilterer.getInstance().applyFilter(s);
                hideFilterButton();

                return false;
            }
        });

        searchView.setOnSuggestionListener(
                new SearchView.OnSuggestionListener() {
                    @Override
                    public boolean onSuggestionSelect(int i) {
                        return false;
                    }

                    @Override
                    public boolean onSuggestionClick(int i) {
                        //Gets the genus name from the cursor
                        String genus = searchView.getSuggestionsAdapter().getCursor().getString(1);
                        MarkerFilterer.getInstance().applyFilter(genus);
                        searchView.setQuery(genus, false);
                        searchView.clearFocus();
                        return true;
                    }
                }
        );

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_about) {
            startActivity(new Intent(MapsActivity.this, AboutActivity.class));
        }
        if (id == R.id.action_toggleSatellite) {
            campusMapFragment.toggleMap();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        MarkerFilterer.getInstance().clearFilter();
    }


    //sets the map fragment.
    private void setMapFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tx = fm.beginTransaction();
        tx.replace(R.id.main_container, fragment);
        tx.commit();
    }

    private void hideFilterButton() {
        Button clearFilterButton = (Button) findViewById(R.id.btnClearFilter);
        clearFilterButton.setVisibility(View.INVISIBLE);

    }

    // applys a filter from a given string, and shows a "Clear Filter" button on screen.
    // This is used from the species list for the "Show on Map" feature.
    public void setFilter(String filterText) {
        Button clearFilterButton = (Button) findViewById(R.id.btnClearFilter);
        MarkerFilterer.getInstance().applyFilter(filterText);
        clearFilterButton.setVisibility(View.VISIBLE);
        clearFilterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideFilterButton();
                MarkerFilterer.getInstance().clearFilter();
            }
        });
    }
}
