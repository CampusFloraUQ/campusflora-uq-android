/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora.floraAPI;

import android.util.Pair;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Species {

    private String genusSpecies, commonName, familyName,
            authority, description, distribution, information, phylogeny;
    private List<Pair<LatLng, String>> locations;
    private List<ArrayList<String>> images;
    private int id, familyID;

    public Species(int id, String genusSpecies, String commonName,
                   String authority, String description,
                   String distribution, String information) {
        this.id = id;
        this.genusSpecies = genusSpecies;
        this.commonName = commonName;
        this.authority = authority;
        this.description = description;
        this.distribution = distribution;
        this.information = information;
        this.locations = new LinkedList<>();

        //Initialize images arrayList, each image will have 5 different URLS so make 5 slots for them
        images = new ArrayList<>();
    }

    /**
     * Setters
     */

    //Add an image
    public void addImage(int imgNum, String url) {
        if (images.size() <= imgNum) {
            ArrayList<String> imageURLS = new ArrayList<String>();
            imageURLS.add(url);
            images.add(imageURLS);
        } else {
            images.get(imgNum).add(url);
        }
    }

    //Adds to the array of lat/lon
    public void addLocation(double lat, double lon, String arborplan) {
        locations.add(
                new Pair<>(
                        new LatLng(lat, lon),
                        arborplan
                )
        );
    }

    //Adds the family of the species, along with it's family ID:
    public void addFamily(int familyID, String familyName, String phylogeny) {
        this.familyName = familyName;
        this.familyID = familyID;
        this.phylogeny = phylogeny;
    }

    /**
     * Getters
     */

    //imgNum is used as there are multiple images per plant. Type is for which type of image is needed. Only the size changes
    public String getImageUrl(int imgNum, String type) {
        //Return null if invalid imgnum or if the images array is empty for this species
        if (imgNum > images.size() || imgNum < 0) {
            return null;
        }
        if (images.size() == 0) {
            return null;
        }
        switch (type) {
            case "desktop":
                return images.get(imgNum).get(1);
            case "mobile":
                return images.get(imgNum).get(2);
            case "thumb_retina":
                return images.get(imgNum).get(3);
            case "thumb":
                return images.get(imgNum).get(4);
            case "default":
                return images.get(imgNum).get(0);
            default:
                return images.get(imgNum).get(0);
        }
    }

    public List<ArrayList<String>> getImages() {
        return this.images;
    }

    public List<Pair<LatLng, String>> getLocations() {
        return locations;
    }

    public int getID() {
        return this.id;
    }

    public int getFamilyID() {
        return this.getFamilyID();
    }

    public String getGenusSpecies() {
        return genusSpecies;
    }

    public String getCommonName() {
        return commonName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getAuthority() {
        return authority;
    }

    public String getDescription() {
        return description;
    }

    public String getDistribution() {
        return distribution;
    }

    public String getInformation() {
        return information;
    }

    public String getPhylogeny() {
        return phylogeny;
    }
}