/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora.floraAPI;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by michael on 17/09/15.
 */
public class Trail {
    private String title;
    private Map<String, Species> species;
    private int id;

    public Trail(int id, String title) {
        this.id = id;
        this.title = title;
        this.species = new HashMap<>();
    }


    public String getTitle() {
        return title;
    }

    public Map<String, Species> getSpecies() {
        return species;
    }

    public void addSpecies(Species species) {
        this.species.put(species.getGenusSpecies(), species);
    }
}
