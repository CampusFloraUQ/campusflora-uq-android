/*
 * Copyright (C) 2015 Campus Flora Android Team
 * Campus Flora Android Team : Michael Johnston, Ahmed Jamal Shadid, Alex Ling,
 * Simong Baeg, Kevin Ahn, Scott Dong, Liam Huang
 *
 * This file is part of Campus Flora Android.
 *
 * Campus Flora Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Campus Flora Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Campus Flora Android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.universityofsydney.campusflora.floraAPI;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

//Serializable to we can pass FloraAPI objects to other classes
public class FloraAPI {
    private static FloraAPI instance; //Private instance of FloraAPI -> Singleton
    private Context context; // needed to access assets folder
    private Map<String, Species> speciesHashMap;
    private int numberOfPlants;
    private Map<Integer, Species> speciesIDMap;

    // Stores the number of species in a given families.
    private Map<String, Integer> familyCount;

    // List of all trails
    private List<Trail> trails;
//    private List<Species> species; //Used for testAPI

    //Private constructor for singleton:
    private FloraAPI() {
//        this.species = new ArrayList<Species>(); //Used for TestAPI
        this.speciesIDMap = new HashMap<>();
        this.speciesHashMap = new HashMap<>();
        this.familyCount = new HashMap<>();
    }

    public static FloraAPI getInstance() {
        if (instance == null) {
            instance = new FloraAPI();
        }
        return instance;
    }

    public boolean isLoaded() {
        return !(context == null);
    }

    //Must be called atleast once before this API is usable.
    public void init(Context context) {
        this.context = context;
        this.loadSpecies();
        try {
            this.loadTrails();
        } catch (JSONException e) {
            Log.e(context.getPackageName(), "Error Loading trails");
        }
    }

    //here for legacy reasons,
    //please adapt methods to using getSpecies(genus)
    //and getGenusNames()
    @Deprecated
    public Map<String, Species> getSpecies() {
        if (this.speciesHashMap == null) {
            this.loadSpecies();
        }
        return this.speciesHashMap;
    }

    public Species getSpecies(String genusName) {
        return this.speciesHashMap.get(genusName);
    }

    public List<Trail> getTrails() {
        return trails;
    }

    public Set<String> getGenusNames() {
        return this.speciesHashMap.keySet();
    }

    private void loadSpecies() {
        //If we have already created the hashMap, return cached copy
        if (context == null || (this.speciesHashMap != null && !this.speciesHashMap.isEmpty())) {
            return;
        }

        JSONArray jsonSpecies = null;
        try {
            jsonSpecies = new JSONArray(this.getJSON("species.json"));
        } catch (JSONException e) {
            Log.e(context.getPackageName(), "JSON Error");
            Log.e(context.getPackageName(), e.toString());
        } catch (IOException e) {
            Log.e(context.getPackageName(), "IO Exception");
            Log.e(context.getPackageName(), e.toString());
        }
        //numberOfPlants = jsonSpecies.length();
        numberOfPlants = 0;
        //For each JSONObject we create a Species object
        for (int i = 0; i < jsonSpecies.length(); i++) {
            JSONObject currObj;
            try {
                //Get the current object and set the required fields for the constructor
                currObj = jsonSpecies.getJSONObject(i);

                Species species = new Species(
                        Integer.parseInt(currObj.getString("id")),
                        currObj.getString("genusSpecies"),
                        currObj.getString("commonName"),
                        currObj.getString("authority"),
                        currObj.getString("description"),
                        currObj.getString("distribution"),
                        currObj.getString("information")
                );

                //Add the family details
                JSONObject family_obj = currObj.getJSONObject("family");

                species.addFamily(
                        Integer.parseInt(family_obj.getString("id")),
                        family_obj.getString("name"),
                        family_obj.getString("phylogeny")
                );

                //Get an array of the locations and set the lat/longs inside the species obj
                JSONArray locations_object = currObj.getJSONArray("species_locations");

                try {
                    for (int j = 0; j < locations_object.length(); j++) {
                        species.addLocation(
                                Double.parseDouble(
                                        locations_object.getJSONObject(j).getString("lat")
                                ),
                                Double.parseDouble(
                                        locations_object.getJSONObject(j).getString("lon")
                                ),
                                locations_object.getJSONObject(j).getString("arborplan_id")
                        );

                        // increment our count for total number of markers on map.
                        numberOfPlants++;

                        // increment our family species count
                        if (this.familyCount.containsKey(species.getFamilyName())) {
                            this.familyCount.put(
                                    species.getFamilyName(),
                                    this.familyCount.get(species.getFamilyName()) + 1
                            );
                        } else {
                            this.familyCount.put(species.getFamilyName(), 1);
                        }
                    }
                } catch (NumberFormatException e) {
                    Log.e(context.getPackageName(), "error parsing lat,long, species:" + species.getGenusSpecies());
                }


                //Also get the image URLS:
                JSONArray image_obj = currObj.getJSONArray("images");
                Log.d("size", Integer.toString(image_obj.length()));
                //Must append this to the image URL to get the real URL
                String prefix = "http://campusflora.sydneybiology.org";
                for (int j = 0; j < image_obj.length(); j++) {
                    //Add all the URLS in for the different types of images
                    String url = prefix + image_obj.getJSONObject(j).getString("image_url");
                    species.addImage(j, url);
                    url = prefix + image_obj.getJSONObject(j).getString("image_url_desktop");
                    species.addImage(j, url);
                    url = prefix + image_obj.getJSONObject(j).getString("image_url_mobile");
                    species.addImage(j, url);
                    url = prefix + image_obj.getJSONObject(j).getString("image_url_thumb_retina");
                    species.addImage(j, url);
                    url = prefix + image_obj.getJSONObject(j).getString("image_url_thumb");
                    species.addImage(j, url);
                }
//                species.add(species); //Use for testAPI
                this.speciesHashMap.put(currObj.getString("genusSpecies"), species);
                this.speciesIDMap.put(species.getID(), species);

            } catch (JSONException e) {
                Log.e(context.getPackageName(), "JSON Error");
                Log.e(context.getPackageName(), e.toString());
                e.printStackTrace();
            }
        }
    }

    public Map<String, Integer> getFamilyCount() {
        return this.familyCount;
    }

    private void loadTrails() throws JSONException {
        //If we have already created the hashMap, return cached copy
        if (context == null || this.trails != null) {
            return;
        }

        JSONArray jsonTrails = null;
        try {
            jsonTrails = new JSONArray(this.getJSON("trails.json"));
        } catch (JSONException e) {
            Log.e(context.getPackageName(), "JSON Error");
            Log.e(context.getPackageName(), e.toString());
        } catch (IOException e) {
            Log.e(context.getPackageName(), "IO Exception");
            Log.e(context.getPackageName(), e.toString());
        }


        this.trails = new LinkedList<>();

        for (int i = 0; i < jsonTrails.length(); i++) {
            JSONObject trailsObject = null;
            try {
                trailsObject = jsonTrails.getJSONObject(i);
            } catch (JSONException e) {
                Log.e(context.getPackageName(), "Error getting Trails object");
                e.printStackTrace();
            }

            Trail trail = null;
            try {
                trail = new Trail(
                        Integer.parseInt(trailsObject.getString("id")),
                        trailsObject.getString("name")
                );

            } catch (NullPointerException e) {
                Log.e(context.getPackageName(), e.toString());
            }

            JSONArray trailsSpeciesArray = trailsObject.getJSONArray("species");
            for (int j = 0; j < trailsSpeciesArray.length(); ++j) {
                JSONObject speciesIDObject = null;
                speciesIDObject = trailsSpeciesArray.getJSONObject(j);

                int id = 0;
                try {
                    id = speciesIDObject.getInt("id");
                    trail.addSpecies(speciesIDMap.get(id));
                } catch (NullPointerException e) {
                    Log.e(context.getPackageName(), "Error getting " + id + " From speciesIDObject");
                    e.printStackTrace();
                }

            }
            trails.add(trail);
        }
    }


    public Drawable getLocalThumbnail(String genusName) {
        if (!this.isLoaded()) {
            Log.e(context.getPackageName(), "FloraAPI: Attempted to load without init()");
            return null;
        }
        Species specie = speciesHashMap.get(genusName);
        if (specie.getImageUrl(0, "thumb_retina") == null) {
            return null;
        }
        String url = specie.getImageUrl(0, "thumb_retina").split("\\?")[0].toLowerCase();
        String prefix = "thumb_retina_";
//        Log.d("URL name: ", url);
        //Fix for these 2 dodgy image names in JSON file.
        if (url.equals("http://campusflora.sydneybiology.org/assets/species_images/ficinia%c2%a0nodosa/483/thumb_retina_ficinia%c2%a0nodosa1.jpg")) {
            url = "species_images/thumb_retina_ficinia_nodasa1.jpg";
        } else if (url.equals("http://campusflora.sydneybiology.org/assets/species_images/canna%c2%a0x%20generalis/482/thumb_retina_canna%c2%a0x_generalis1.jpg")) {
            url = "species_images/thumb_retina_cannax_generalis1.jpg";
        } else {
            url = "species_images/" + prefix + url.split(prefix)[1];
        }

        //thumb_retina_cannaÂ x_generalis1.jpg
        //thumb_retina_ficiniaÂ nodosa1.jpg

        try {
            InputStream istr = context.getAssets().open(url);
            Drawable ret = Drawable.createFromStream(istr, null);
            return ret;
        } catch (IOException e) {
            //e.printStackTrace();
            //Log.e(context.getPackageName(), e.toString());
        }
        //Log.e (context.getPackageName(), "could not find image for " + genusName +  " url=" + url);

        return null;
    }

    private String getJSON(String filename) throws IOException {
        InputStream is = null;
        try {
            is = context.getAssets().open(filename);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(context.getPackageName(), "Error opening " + filename + "from assets");
        }
        BufferedReader br = new BufferedReader(
                new InputStreamReader(is)
        );
        String json = "";
        String line = br.readLine();
        while (line != null) {
            json += line;
            line = br.readLine();
        }
        return json;
    }

    public int getNumberOfFamilies() {
        Set<String> familiesCounted = new HashSet<>();
        for (String genus : getGenusNames()) {
            Species species = getSpecies(genus);
            String familyName = species.getFamilyName();
            familiesCounted.add(familyName);
        }
        return familiesCounted.size();
    }

    public int getNumberOfPlants() {
        return numberOfPlants;
    }
    //Used for TestAPI
//    public List<Species> getSpeciesList(){
//        return this.species;
//    }
}
