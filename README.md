# Readme

This is a fork of the original CampusFlora Android app.

The Mercurial (Hg) repository was converted to the Git revision system. The original Hg repository is: https://bitbucket.org/sydneyuni/campus-flora-android

Below is the original Readme.

##  Campus Flora Android
Campus Flora maps the locations of individual plants provides botanical information on each species. Trails highlight important aspects of select plant groups and align with the current botanical curriculum.

Campus Flora extends the teaching of botany from the classroom into the University of Sydney campus grounds and enables us to share our learning resources with the broader community.

> [Play Store link](https://play.google.com/store/apps/details?id=com.universityofsydney.campusflora)  
> [Homepage](https://campusflora.wordpress.com)  

This code is released free and opensource under GPLv3
![Alt text](https://i.imgur.com/iFAfzZO.png "Screenshot of Application")


